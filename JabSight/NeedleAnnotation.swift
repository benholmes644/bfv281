//
//  NeedleAnnotation.swift
//  SeismographSwift
//
//  Created by Nikolay Diyanov on 12/22/14.
//  Copyright (c) 2014 Telerik. All rights reserved.
//

import Foundation
import CoreGraphics
import CoreMotion

class NeedleAnnotation : TKChartPointAnnotation {
    
    var center: CGPoint = CGPoint.zero
    
     func layoutInRect(_ bounds: CGRect) {
        let xval = self.series!.xAxis!.numericValue(self.position.dataXValue)
        let x = TKChartSeriesRender.location(ofValue: xval, for: self.series!.xAxis!, in: bounds)
        let yval = self.series!.yAxis!.numericValue(self.position.dataYValue)
        let y = TKChartSeriesRender.location(ofValue: yval, for: self.series!.yAxis!, in: bounds)
        center = CGPoint(x: x, y: y)
    }
    
    override func draw(in context: CGContext) {
        context.beginPath()
        context.move(to: CGPoint(x: center.x-20, y: center.y))
        context.addLine(to: CGPoint(x: center.x+20, y: center.y+20))
        context.addLine(to: CGPoint(x: center.x+20, y: center.y-20))
        
        context.setFillColor(red: 0, green: 0, blue: 0, alpha: 1)
        context.fillPath()
    }
}
