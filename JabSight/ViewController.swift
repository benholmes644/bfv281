//
//  ViewController.swift
//  JabSight
//
//  Created by Amrit Sidhu on 25/03/17.
//  Copyright © 2017 Amrit Sidhu. All rights reserved.
//

import UIKit
import AVFoundation
import CoreGraphics
import CoreMotion

enum AxisType : Int {
    case x = 0
    case y = 1
    case z = 2
}

let coefficient : Double = 20

class ViewController: UIViewController {
    
    //Telerik Graph
    @IBOutlet var chart: TKChart!
    @IBOutlet weak var viewScreenshot: UIView!
    var dataPoints = [TKChartDataPoint]()
    var aType = AxisType.z
    let motionManager = CMMotionManager()
    
    //Camera View
    @IBOutlet weak var previewView: UIView!
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var audioPlayer = AVAudioPlayer()
    
    @IBOutlet weak var imgViewScreenShot: UIImageView!
    var isAlertTitle = false

    var uploadData: Data?

    var connection: DLSFTPConnection?
    var request: DLSFTPRequest?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewScreenshot.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupCameraView()
        for _ in 0..<10 {
            self.buildChartWithPoint(coefficient * 0.0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if previewLayer != nil {
            previewLayer!.frame = previewView.bounds
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        audioPlayer.stop()
    }
    
    //------------------- Server upload --------------------------------

    func uploadDLSFTP() {
        // make a connection object and attempt to connect
        let connection = DLSFTPConnection(hostname: "server1.stora2.co", port: 22, username: "storyeaw", password: "%bCs3Pw-Z$z#")
        self.connection = connection
        let successBlock = {() -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                // login successful
                //let URL = NSBundle.mainBundle().pathForResource("TestUpload", ofType: "png")
                self.request = DLSFTPUploadRequest(remotePath: "/home/storyeaw/public_html/dslruploads/image\(UUID().uuidString).png", localPath: self.getImage(), successBlock: nil, failureBlock: nil, progressBlock: nil)
                self.connection!.submitRequest(self.request)
            })
        }

        connection?.connect(successBlock: successBlock, failureBlock: nil)
    }

    func saveImageDocumentDirectory(_ image: UIImage) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.jpg")
        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }

    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func getImage() -> String {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("apple.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            return imagePAth
        }else{
            return ""
        }
    }

    //-------------------------------------------
    
    func playSound(_ soundName: String)
    {
        let coinSound = URL(fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "mp3")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf:coinSound)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch {
            print("Error getting the audio file")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var tField: UITextField!
    
    func configurationTextField(_ textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Capture Name"
        tField = textField
    }


    @IBAction func btnInitilizeTapped(_ sender: AnyObject) {
        
        playSound("initialize")
        viewScreenshot.isHidden = true
        
        if !isAlertTitle {
            isAlertTitle = true
            let alert = UIAlertController(title: "JabSight", message: "", preferredStyle: .alert)
            
            alert.addTextField(configurationHandler: configurationTextField)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
                print("Done !!")
                if self.tField.text?.characters.count != 0 {
                    SVProgressHUD.show()
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                        SVProgressHUD.dismiss()
                        self.startOperations()
                    })
                }
                
                print("Item : \(self.tField.text)")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        } else {
            let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                self.startOperations()
            })
        }

    }
    
    @IBAction func btnHoldStreamTapped(_ sender: AnyObject) {
        motionManager.stopAccelerometerUpdates()
        OperationQueue.current!.cancelAllOperations()
        
        viewScreenshot.isHidden = false
        imgViewScreenShot.image = previewView.image()
    }
    
    @IBAction func btnEndTapped(_ sender: AnyObject) {
        playSound("end")
        SVProgressHUD.show()
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            // your function here
            self.dataPoints.removeAll(keepingCapacity: true)
            self.chart.removeAllData()
            SVProgressHUD.dismiss()
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            for aViewController:UIViewController in viewControllers {
                if aViewController.isKind(of: LoginViewController.self) {
                    _ = self.navigationController?.popToViewController(aViewController, animated: true)
                }
            }
        })
    }
    
    func randomBetweenNumbers(_ firstNum: CGFloat, secondNum: CGFloat) -> CGFloat{
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNum - secondNum) + min(firstNum, secondNum)
    }
    
    func startOperations() {
//        for i in 0..<1000 {
//            let dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(Double(i/1000) * Double(NSEC_PER_SEC)))
//            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
//                // your function here
//                self.buildChartWithPoint(coefficient * Double(self.randomBetweenNumbers(-1, secondNum: 1)))
//            })
//            
//        }
        let imageScreenshot = previewView.image()
        saveImageDocumentDirectory(imageScreenshot!)
        uploadDLSFTP()

        motionManager.accelerometerUpdateInterval = 0.2
        if motionManager.isAccelerometerAvailable {
            motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: { accelerometerData, error in
                
                let acceleration = accelerometerData!.acceleration
                
                switch self.aType {
                case .x:
                    self.buildChartWithPoint(coefficient * acceleration.x)
                case .y:
                    self.buildChartWithPoint(coefficient * acceleration.y)
                case .z:
                    self.buildChartWithPoint(coefficient * acceleration.z)
                }
            })
        }
    }
    
    func buildChartWithPoint(_ point: Double) {
        self.chart.removeAllData()
        self.chart.removeAllAnnotations()
        
        let lastPoint = TKChartDataPoint(x: Date(), y: point)
        dataPoints.append(lastPoint)
        
        if dataPoints.count > 25 {
            dataPoints.remove(at: 0)
        }
        
        let yAxis = TKChartNumericAxis(minimum: -30, andMaximum: 30)
        yAxis.position = TKChartAxisPosition.left
        yAxis.majorTickInterval = 10
        yAxis.minorTickInterval = 1
        yAxis.offset = 0
        yAxis.baseline = 0
        yAxis.style.labelStyle.fitMode = TKChartAxisLabelFitMode.none
        yAxis.style.labelStyle.firstLabelTextAlignment = TKChartAxisLabelAlignment.left
        self.chart.yAxis = yAxis
        
        let lineSeries = TKChartLineSeries(items: dataPoints)
        lineSeries.style.palette = TKChartPalette()
        let strokeRed = TKStroke(color: UIColor(red: 13.0/255.0, green: 178.0/255.0, blue: 225.0/255.0, alpha: 1.0), width: 1.6)
        lineSeries.style.palette!.addItem(TKChartPaletteItem(stroke: strokeRed))
        chart.addSeries(lineSeries)
        
        
        
        let dashStroke = TKStroke(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5), width: 0.5)
        dashStroke.dashPattern = [6, 1]
        
//        let axisColor = TKStroke(color: UIColor.blackColor(), width: 1)
        chart.xAxis!.style.lineStroke = dashStroke
        chart.xAxis!.style.majorTickStyle.ticksHidden = false
        chart.xAxis!.style.labelStyle.textHidden = true
        
        let annotationBandRed = TKChartBandAnnotation(range: TKRange(minimum: -30, andMaximum: 30), for: chart.yAxis!)
        annotationBandRed.style.fill = TKSolidFill(color: UIColor.white)
        chart.addAnnotation(annotationBandRed)
        
        let annotationBandYellow = TKChartBandAnnotation(range: TKRange(minimum: -20, andMaximum: 20), for: chart.yAxis!)
        annotationBandYellow.style.fill = TKSolidFill(color: UIColor.white)
        chart.addAnnotation(annotationBandYellow)
        
        let annotationBandGreen = TKChartBandAnnotation(range: TKRange(minimum: -10, andMaximum: 10), for: chart.yAxis!)
        annotationBandGreen.style.fill = TKSolidFill(color: UIColor.white)
        chart.addAnnotation(annotationBandGreen)
        
        let positiveDashAnnotation = TKChartGridLineAnnotation(value: 10, for: chart.yAxis!)
        positiveDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(positiveDashAnnotation)
        
        let negativeDashAnnotation = TKChartGridLineAnnotation(value: -10, for: chart.yAxis!)
        negativeDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(negativeDashAnnotation)
        
        let positiveDashAnnotation1 = TKChartGridLineAnnotation(value: 20, for: chart.yAxis!)
        positiveDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(positiveDashAnnotation1)
        
        let negativeDashAnnotation1 = TKChartGridLineAnnotation(value: -20, for: chart.yAxis!)
        negativeDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(negativeDashAnnotation1)
        
        let positiveDashAnnotation2 = TKChartGridLineAnnotation(value: 30, for: chart.yAxis!)
        positiveDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(positiveDashAnnotation2)
        
        let negativeDashAnnotation2 = TKChartGridLineAnnotation(value: -30, for: chart.yAxis!)
        negativeDashAnnotation.style.stroke = dashStroke
        chart.addAnnotation(negativeDashAnnotation2)
        
//        if dataPoints.count > 1 {
//            let needle = NeedleAnnotation(point:lastPoint, forSeries: lineSeries)
//            needle.zPosition = TKChartAnnotationZPosition.AboveSeries
//            chart.addAnnotation(needle)
//        }
    }
    
    //MARK: Initilize camera
    func setupCameraView() {
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addOutput(stillImageOutput)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(previewLayer!)
                
                captureSession!.startRunning()
            }
        }
    }
}

extension UIView {
    
    class func image(_ view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        guard let ctx = UIGraphicsGetCurrentContext() else {
            return nil
        }
        view.layer.render(in: ctx)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    func image() -> UIImage? {
        return UIView.image(self)
    }
}
