//
//  OnboardingViewController.swift
//  JabSight
//
//  Created by Amrit Sidhu on 26/03/17.
//  Copyright © 2017 Amrit Sidhu. All rights reserved.
//

import UIKit
import AVFoundation

class OnboardingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionViewPages: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var audioPlayer = AVAudioPlayer()
    
    var arrLogo = ["graphic1", "graphic2", "graphic3", "graphic4"]
    var arrTitle = ["Welcome to JabSight!", "Secure Your Phone", "Initialize Your Capture", "Upload Your DSLR Media"]
    var arrDesc = ["The world's simplest capture utility for 3D home tour creation.", "Before beginning, make sure your phone is securely affixed to the camera's hotshoe mount.", "JabSight simultaneously records live video and accelerometry feeds during the shoot. Select 'Initialize' to begin capturing.", "When the shoot is completed, login to Jabsight.com upload your shoot media, and select a processing option. \n You'll be notified via email when your tour is ready!"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        pageControl.numberOfPages = arrLogo.count
        playSound()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        audioPlayer.stop()
    }
    
    func playSound()
    {
        let coinSound = URL(fileURLWithPath: Bundle.main.path(forResource: "appchillo", ofType: "mp3")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf:coinSound)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch {
            print("Error getting the audio file")
        }
    }
    
    @IBAction func btnLoginTaped(_ sender: AnyObject) {
    }
    
    // MARK: UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLogo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let imageLogo = cell.viewWithTag(1001) as? UIImageView {
            imageLogo.image = UIImage(named: arrLogo[indexPath.row])
        }
        if let lblTitle = cell.viewWithTag(1002) as? UILabel {
            lblTitle.text = arrTitle[indexPath.row]
        }
        if let lblDesc = cell.viewWithTag(1003) as? UILabel {
            lblDesc.text = arrDesc[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
       
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
         pageControl.currentPage = indexPath.row
    }
    
    // MARK: UICollectionViewFlowLayout Delegate
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
