//
//  LoginViewController.swift
//  JabSight
//
//  Created by Amrit Sidhu on 26/03/17.
//  Copyright © 2017 Amrit Sidhu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var txtFldUsername: UITextField!
    @IBOutlet weak var txtFldPassword: UITextField!
    @IBOutlet weak var txtFldIP: UITextField!
    @IBOutlet weak var viewIP: UIView!
    @IBOutlet weak var btnCloudIP: UIButton!
    @IBOutlet weak var btnLocalIP: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnCloudIP.setTitleColor(UIColor.blue, for: UIControlState())
        btnLocalIP.setTitleColor(UIColor.black, for: UIControlState())
        viewIP.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
        txtFldPassword.text = ""
        txtFldUsername.text = ""
        txtFldIP.text = ""
    }

    @IBAction func btnStreamTapped(_ sender: AnyObject) {
        loginTapped()
    }
    
    @IBAction func btnCloudTapped(_ sender: AnyObject) {
        viewIP.isHidden = true
        btnCloudIP.setTitleColor(UIColor.blue, for: UIControlState())
        btnLocalIP.setTitleColor(UIColor.black, for: UIControlState())
    }
    @IBAction func btnLocalTapped(_ sender: AnyObject) {
        viewIP.isHidden = false
        btnCloudIP.setTitleColor(UIColor.black, for: UIControlState())
        btnLocalIP.setTitleColor(UIColor.blue, for: UIControlState())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("TextField did begin editing method called")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("TextField did end editing method called\(textField.text)")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        print("TextField should begin editing method called")
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("TextField should clear method called")
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        loginTapped()
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func loginTapped() {
        if txtFldUsername.text == "admin" && txtFldPassword.text == "pass" {
            SVProgressHUD.show()
            let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                // your function here
                SVProgressHUD.dismiss()
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            })
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
